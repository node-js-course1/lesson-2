// eslint-disable-next-line node/no-extraneous-import
import { describe, expect, test } from '@jest/globals';
import validatePath from '../utils/validatePath';

describe('validatePath', () => {
  test('Valid path with relative path', async () => {
    const path = './images/';
    const result = await validatePath(path);
    expect(result).toBe(true);
  });

  test('Valid path with absolute path', async () => {
    const path = process.cwd();
    const result = await validatePath(path);
    expect(result).toBe(true);
  });

  test('Not existing path', async () => {
    const path = './invalid/path';
    await expect(validatePath(path)).rejects.toThrowError(
      `Path ${path} does not exist`
    );
  });

  test('Not directory. Direction to file as path', async () => {
    const path = process.cwd() + '/package.json';
    await expect(validatePath(path)).rejects.toThrowError(
      `Path "${path}" is not a directory`
    );
  });

  test('missing path', async () => {
    const path = '';
    await expect(validatePath(path)).rejects.toThrowError('Path is required');
  });
});
