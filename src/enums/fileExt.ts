enum FileExt {
  WEBP = '.webp',
  PNG = '.png',
  JPG = '.jpg',
  JPEG = '.jpeg',
}

export default FileExt;
