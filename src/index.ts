import { Command } from 'commander';
import convertImages from './utils/convertImages';
const path = require('path');

const program = new Command();

program
  .version('0.1.0')
  .description('Converts PNG files to WebP files')
  .option('-i, --input <inputPath>', 'Path to the input directory')
  .option('-o, --output <outputPath>', 'Path to the output directory')
  .option(
    '-e --extensions <extensions>',
    'List of file extensions to convert. Separate with comma. Without dot.'
  )
  .action(async () => {
    const inputPath = path.resolve(program.opts().input);
    const outputPath = path.resolve(program.opts().output);
    const extensions = program.opts().extensions;

    await convertImages(inputPath, outputPath, extensions);
  });

program.parse(process.argv);
