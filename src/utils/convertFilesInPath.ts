import convertToFileExt from './convertToFileExt';
import FileExt from '../enums/fileExt';

const { WEBP } = FileExt;
const path = require('path');

async function convertFilesInPath(
  files: string[],
  inputPath: string,
  outputPath: string
) {
  const filesCount = files.length;
  await Promise.all(
    files.map(async (file: string, index: number) => {
      console.log(`Progress: ${index + 1}/${filesCount}`);
      const inputFile = path.join(inputPath, file);
      const outputFile = path.join(outputPath, path.parse(file).name + WEBP);

      await convertToFileExt(inputFile, outputFile, WEBP);
    })
  );
}

export default convertFilesInPath;
