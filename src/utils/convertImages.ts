import validatePath from './validatePath';
const fs = require('fs');
import convertFilesInPath from './convertFilesInPath';
import filterOnlyRequstedFileExt from './filterOnlyRequstedFileExt';
import validateExtenstions from './validateExtensions';

async function convertImages(
  inputPath: string,
  outputPath: string,
  extenstions: string
) {
  try {
    await validatePath(inputPath);
    const extenstionsValidated = await validateExtenstions(extenstions);

    createDirIfItDoesntExist(outputPath);
    const files = fs.readdirSync(inputPath);
    console.log(`Total files in path founded: ${files.length}`);

    const filtredFiles = await filterOnlyRequstedFileExt(
      files,
      extenstionsValidated
    );
    console.log(
      `Files with required extensions founded: ${filtredFiles.length}`
    );
    await convertFilesInPath(filtredFiles, inputPath, outputPath);
  } catch (err) {
    console.error(err);
  }
}

function createDirIfItDoesntExist(outputPath: string) {
  if (!fs.existsSync(outputPath)) {
    fs.mkdirSync(outputPath);
  }
}

export default convertImages;
