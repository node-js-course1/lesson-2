import FileExt from '../enums/fileExt';
const sharp = require('sharp');

async function convertToFileExt(
  inputFile: string,
  outputFile: string,
  requiredFileExt: FileExt
) {
  switch (requiredFileExt) {
    case FileExt.WEBP:
      await sharp(inputFile).webp().toFile(outputFile);
      break;
  }
}

export default convertToFileExt;
