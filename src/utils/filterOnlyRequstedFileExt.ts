import FileExt from '../enums/fileExt';

const { PNG } = FileExt;
const path = require('path');

async function filterOnlyRequstedFileExt(
  files: string[],
  allowedExt: string[] = [PNG]
) {
  return files.filter(
    (file: string) =>
      allowedExt.indexOf(path.extname(file).toLowerCase()) !== -1
  );
}

export default filterOnlyRequstedFileExt;
