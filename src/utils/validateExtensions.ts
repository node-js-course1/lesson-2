async function validateExtenstions(extenstions: string) {
  const extensionsArray = extenstions.split(',');

  if (extensionsArray.length === 0) {
    throw new Error('Extensions are required');
  }

  const extensionArrayWithDots = extensionsArray.map((extension: string) =>
    extension.indexOf('.') === -1 ? `.${extension}` : extension
  );

  console.log(
    `Files with extensions "${extensionArrayWithDots}" will be converted to .webp`
  );
  return extensionArrayWithDots;
}

export default validateExtenstions;
