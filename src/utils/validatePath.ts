const fs = require('fs');

async function validatePath(path: string) {
  if (!path) {
    throw new Error('Path is required');
  }
  if (!fs.existsSync(path)) {
    throw new Error(`Path ${path} does not exist`);
  }

  const inputDir = fs.statSync(path);
  if (!inputDir.isDirectory()) {
    throw new Error(`Path "${path}" is not a directory`);
  }

  return true;
}

export default validatePath;
